package com.example.demo2.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PersonTest {

    @BeforeEach
    void setUp() {
        System.out.println("Running before each");
    }

    @Test
    void myTestMethod() {
        System.out.println("Running test 1");
        Person p = new Person("Aishwarya", "Kadra");
        assertEquals(p.getName(), "Aishwarya");

    }

    @Test
    void myTestMethod2() {
        System.out.println("Running test 2");
        assertTrue(true);
    }

}