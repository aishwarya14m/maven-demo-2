package com.example.demo2.service;

import com.example.demo2.model.Person;
import com.example.demo2.repository.PersonRepository;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
class PersonServiceImplTest {

    @Autowired
    PersonRepository personRepository;

    PersonService personService;

    @BeforeEach
    public void setUp() throws Exception {
        personService = new PersonServiceImpl(personRepository);
    }

    @Test
    void getAllPersons() {
        personRepository.save(new Person("A", "L"));
        System.out.println(personRepository.count());
        Assertions.assertTrue(personService.getAllPersons().size() > 0);
        System.out.println("Running getAllPersons");
    }

    @Test
    void createNewPerson() {
        long initialCount = personRepository.count();

        personService.createNewPerson(new Person("Aishwarya", "Kadra"));

        long finalCount = personRepository.count();

        System.out.println("Difference : " + (finalCount - initialCount));

        System.out.println("Running createNewPerson");
        Assert.assertEquals((finalCount - initialCount), 1);
    }
}