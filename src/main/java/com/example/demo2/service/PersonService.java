package com.example.demo2.service;

import com.example.demo2.model.Person;

import java.util.List;


public interface PersonService {
    List<Person> getAllPersons();

    Person createNewPerson(Person person);
}
