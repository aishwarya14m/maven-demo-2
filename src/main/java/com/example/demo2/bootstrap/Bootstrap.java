package com.example.demo2.bootstrap;

import com.example.demo2.model.Person;
import com.example.demo2.repository.PersonRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class Bootstrap implements CommandLineRunner {
    private final PersonRepository personRepository;

    public Bootstrap(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println("Run method......");

        Person p = new Person("Aishwarya", "Kadra");
        personRepository.save(p);

        System.out.println("Total objects in database: " + personRepository.count());

        Person p2 = new Person("Aishwarya2", "Kadra");
        personRepository.save(p2);

        System.out.println("Total objects in database: " + personRepository.count());

    }
}
